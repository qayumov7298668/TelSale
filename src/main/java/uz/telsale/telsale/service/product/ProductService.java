package uz.telsale.telsale.service.product;

import org.springframework.web.multipart.MultipartFile;
import uz.telsale.telsale.domain.Product;
import uz.telsale.telsale.domain.dtos.ProductDto;
import uz.telsale.telsale.domain.dtos.UserDto.PageList;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.service.BaseService;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

public interface ProductService extends BaseService<Product, ProductDto> {

    Response<?> getPageProductList(PageList pageList);
    Response<?> getPageOwnerProduct(PageList pageList, Principal owner);
    Response<?> deleteProduct(UUID productId, Principal owner);
    Response<?> addProduct(ProductDto productDto, Principal user);
    Response<?> getProductImage(UUID productId);
    Response<?> saveProductImages(List<MultipartFile> productImages, UUID productId, Principal principal);
    Response<?> getProductInfo(UUID productId);
}
