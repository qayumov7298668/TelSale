package uz.telsale.telsale.service.product;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import uz.telsale.telsale.domain.FileEntity;
import uz.telsale.telsale.domain.Product;
import uz.telsale.telsale.exeptions.DataNotFoundException;
import uz.telsale.telsale.repository.FileUploadRepository;
import uz.telsale.telsale.repository.ProductRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService{
    private final ProductRepository productRepository;
    private String dir;
    private final Path fileLocation;
    private final FileUploadRepository fileUploadRepository;
    private final ProductService productService;

    @Autowired
    public FileServiceImpl(ProductRepository productRepository, @Value("${file.upload-dir}") String fileUploadDir, FileUploadRepository fileUploadRepository,@Lazy ProductService productService) {
        this.productRepository = productRepository;
        this.fileLocation = Paths.get(fileUploadDir)
                .toAbsolutePath().normalize();
        this.fileUploadRepository = fileUploadRepository;
        this.productService = productService;
    }

    private String generateUniqueFileName(String fileExtension) {
        String timestamp = LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss_SSS"));
        return timestamp + "." + fileExtension;
    }

    public Path downloadFile(String fileName) {
        return fileLocation.resolve(fileName);
    }

    @Override
    public FileEntity create(MultipartFile file) {
        String originalFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String fileExtension = originalFileName.contains(".") ? originalFileName.substring(originalFileName.lastIndexOf(".") + 1) : "";

        String uniqueFileName = generateUniqueFileName(fileExtension);
        Path targetLocation = fileLocation.resolve(uniqueFileName);

        try {
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Failed to save the file: " + originalFileName, e);
        }

        String downloadUrl = "/uploads/" + uniqueFileName;
        String fileUrl = fileLocation.resolve(uniqueFileName).toString();
        double fileSizeInKB = (double) file.getSize() / 1024;

        return fileUploadRepository.save(FileEntity.builder()
                .fileName(uniqueFileName)
                .fileType(fileExtension)
                .downloadUrl(downloadUrl)
                .fileUrl(fileUrl)
                .size(fileSizeInKB)
                .build());
    }

    @Override
    public FileEntity getById(UUID id) {
        return fileUploadRepository.findById(id).orElseThrow(()-> new DataNotFoundException("File not found by id"));
    }

    @Override
    public FileEntity update(MultipartFile multipartFile, UUID id) {
        return null;
    }

    @Override
    public void deleteById(UUID id) {
        fileUploadRepository.deleteById(id);
    }

    @Override
    public List<String> getPhotos(UUID productId) {
        List<String> imagesLink = new ArrayList<>();
        Product product = productRepository.findById(productId).orElseThrow(()-> new DataNotFoundException("Hotel not found"));
        for (FileEntity file: product.getImages()){
            imagesLink.add(file.getDownloadUrl());
        }
        return imagesLink;
    }

    @Override
    public Path getFile(String filename) {
        return fileLocation.resolve(filename);
    }

    @Override
    public FileEntity createProductImages(MultipartFile multipartFile, UUID productId) {
        FileEntity fileEntity = create(multipartFile);
        Product product = productService.getById(productId);
        fileEntity.setProduct(product);
        return fileUploadRepository.save(fileEntity);
    }
}
