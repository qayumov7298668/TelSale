package uz.telsale.telsale.service.product;

import org.springframework.web.multipart.MultipartFile;
import uz.telsale.telsale.domain.FileEntity;
import uz.telsale.telsale.domain.Product;
import uz.telsale.telsale.service.BaseService;

import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

public interface FileService extends BaseService<FileEntity, MultipartFile> {
    List<String> getPhotos(UUID productId);
    Path getFile(String filename);
    FileEntity createProductImages(MultipartFile multipartFile, UUID productId);
}
