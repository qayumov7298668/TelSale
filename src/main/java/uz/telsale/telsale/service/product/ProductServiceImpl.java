package uz.telsale.telsale.service.product;

import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.telsale.telsale.domain.FileEntity;
import uz.telsale.telsale.domain.Product;
import uz.telsale.telsale.domain.dtos.ProductDto;
import uz.telsale.telsale.domain.dtos.UserDto.PageList;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.domain.dtos.response.Status;
import uz.telsale.telsale.domain.user.Role;
import uz.telsale.telsale.domain.user.User;
import uz.telsale.telsale.exeptions.DataNotFoundException;
import uz.telsale.telsale.exeptions.UserBadRequestException;
import uz.telsale.telsale.repository.ProductRepository;
import uz.telsale.telsale.service.user.UserService;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ModelMapper modelMapper;
    private final ProductRepository productRepository;
    private final UserService userService;
    private final FileService fileService;
    @Override
    public Product create(ProductDto productDto) {
        Product map = modelMapper.map(productDto, Product.class);
        map.setColor(productDto.getColor().toUpperCase());
        return map;
    }

    @Override
    public Product getById(UUID id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Product not found with Id"));
        Hibernate.initialize(product.getImages());
        return product;
    }

    @Override
    public Product update(ProductDto productDto, UUID id) {
        Product product = getById(id);
        product.setAddress(productDto.getAddress());
        product.setBox(productDto.getBox());
        product.setColor(productDto.getColor().toUpperCase(Locale.ROOT));
        product.setCondition(productDto.getCondition());
        product.setExchange(productDto.getExchange());
        product.setPrice(productDto.getPrice());
        product.setStorage(productDto.getStorage());
        product.setBatteryHealth(productDto.getBatteryHealth());
        product.setCustomerTg(productDto.getCustomerTg());
        product.setUpdatedDate(LocalDateTime.now());
        return productRepository.save(product);
    }

    @Override
    public void deleteById(UUID id) {
        productRepository.deleteById(id);
    }

    @Override
    public Response<?> getPageProductList(PageList pageList) {
        try{
            Pageable pageable = pageableInShort(pageList.getPage(), pageList.getSize(), pageList.getSortBy(), Sort.Direction.valueOf(pageList.getSortDirection()));
            Page<Product> all = productRepository.findAllBy(pageable);
            return Response.builder()
                    .status(Status.SUCCESS).msg("Get all products").data(all.getContent()).build();
        }catch (Exception e){
            throw new DataNotFoundException("Something wrong: "+e);
        }
    }

    @Override
    public Response<?> getPageOwnerProduct(PageList pageList, Principal owner) {
        try{
            Pageable pageable = pageableInShort(pageList.getPage(), pageList.getSize(), pageList.getSortBy(), Sort.Direction.valueOf(pageList.getSortDirection()));
            User user = userService.getByUsername(owner.getName());
            Page<Product> allByOwnerId = productRepository.findProductByOwnerId(user.getId(), pageable);
            return Response.builder().status(Status.SUCCESS).msg("Owner product").data(allByOwnerId).build();
        }catch (Exception e){
            throw new DataNotFoundException("Something wrong: "+e);
        }
    }

    @Override
    public Response<?> deleteProduct(UUID productId, Principal owner) {
        User user = userService.getByUsername(owner.getName());
        Product product = getById(productId);
        if(product.getOwnerId().equals(user.getId())||user.getRole().equals(Role.ADMIN)) {
            deleteById(productId);
            return Response.builder().status(Status.SUCCESS).msg("Product deleted").build();
        }throw new DataNotFoundException("You cannot delete: "+product.getName());
    }

    @Override
    public Response<?> addProduct(ProductDto productDto, Principal user) {
        User owner = userService.getByUsername(user.getName());
        Product product = create(productDto);
        product.setOwnerId(owner.getId());
        return Response.builder().status(Status.SUCCESS).msg("Saved product").data(productRepository.save(product)).build();
    }

    @Override
    public Response<?> getProductImage(UUID productId) {
        List<String> photos = fileService.getPhotos(productId);
        return Response.builder().status(Status.SUCCESS).msg("Get product image").data(photos).build();
    }

    @Override
    @Transactional
    public Response<?> saveProductImages(List<MultipartFile> productImages, UUID productId, Principal principal) {
        List<FileEntity> fileEntities = saveImage(productImages, productId);
        Product product = getById(productId);
        User user = userService.getByPhoneNumberOrUsername(principal.getName());
        if(user.getId().equals(product.getOwnerId())||user.getRole().equals(Role.ADMIN)){
            product.setImages(fileEntities);
            productRepository.save(product);
            return Response.builder().msg("Saved image to "+product.getName()).build();
        }throw new UserBadRequestException("User cannot save image: is not owner");
    }

    @Override
    public Response<?> getProductInfo(UUID productId) {
        Product product = getById(productId);
        return Response.builder().status(Status.SUCCESS).msg("Get product info").data(product).build();
    }

    private Pageable pageableInShort(int page, int size, String sortField, Sort.Direction sortDirection){
        Sort sort = Sort.by(sortDirection, sortField);
        return PageRequest.of(page, size, sort);
    }

    private List<FileEntity> saveImage(List<MultipartFile> files, UUID productId) {
        if(!files.isEmpty()){
            List<FileEntity> images = new ArrayList<>();
            for (MultipartFile file : files) {
                try {
                    FileEntity image = fileService.createProductImages(file, productId);
                    images.add(image);
                } catch (Exception e) {
                    throw new DataNotFoundException("Exception when saving image: " + e);
                }
            }
            return images;
        }return null;
    }
}
