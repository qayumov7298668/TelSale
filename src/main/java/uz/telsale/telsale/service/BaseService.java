package uz.telsale.telsale.service;

import java.security.Principal;
import java.util.UUID;

public interface BaseService<T, R> {
    T create(R r);
    T getById(UUID id);
    T update(R r, UUID id);
    void deleteById(UUID id);
}
