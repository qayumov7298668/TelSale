package uz.telsale.telsale.service.user;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import uz.telsale.telsale.domain.user.User;

public interface JwtService {
    String generateAccessToken(User userEntity);
    String generateRefreshToken(User userEntity);
    User getUserByToken (String token);
    Jws<Claims> extractToken(String token);
}
