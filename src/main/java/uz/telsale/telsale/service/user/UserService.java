package uz.telsale.telsale.service.user;

import uz.telsale.telsale.domain.dtos.UserDto.LoginDto;
import uz.telsale.telsale.domain.dtos.UserDto.PageList;
import uz.telsale.telsale.domain.dtos.UserDto.UserDto;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.domain.user.User;
import uz.telsale.telsale.service.BaseService;

import javax.naming.AuthenticationException;
import java.security.Principal;
import java.util.UUID;

public interface UserService extends BaseService<User, UserDto> {
    Response<?> register(UserDto user);
    User getByUsername(String username);
    User getByPhoneNum(String phoneNumber);
    User getByPhoneNumberOrUsername(String numberOrUsername);
    Response<?> login(LoginDto loginDto) throws AuthenticationException;
    Response<?> getAllUser(PageList pageUserList);
    Response<?> updateUser(UserDto userDto, UUID id, Principal principal);
    Response<?> getUserInfo(Principal principal);
    Response<?> deleteUser(UUID userId, Principal principal);
    Response<?> newTokenByOld(String token);
    Response<?> setNewPassword(String phone, String newPassword);
    Response<?> verifyUser(String phoneNumber, int code);
    Response<?> sendVerifyCode(String phoneNumber);
    Response<?> getUserInfoById(UUID userId);
}
