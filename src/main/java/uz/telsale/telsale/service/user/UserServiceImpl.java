package uz.telsale.telsale.service.user;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.telsale.telsale.domain.AuthEntity;
import uz.telsale.telsale.domain.dtos.UserDto.LoginDto;
import uz.telsale.telsale.domain.dtos.UserDto.PageList;
import uz.telsale.telsale.domain.dtos.UserDto.UserDto;
import uz.telsale.telsale.domain.dtos.response.JwtResponse;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.domain.dtos.response.Status;
import uz.telsale.telsale.domain.user.Role;
import uz.telsale.telsale.domain.user.State;
import uz.telsale.telsale.domain.user.User;
import uz.telsale.telsale.exeptions.AuthenticationFailedException;
import uz.telsale.telsale.exeptions.DataNotFoundException;
import uz.telsale.telsale.exeptions.DuplicateKeyException;
import uz.telsale.telsale.exeptions.UserRegistrationException;
import uz.telsale.telsale.repository.AuthRepository;
import uz.telsale.telsale.repository.UserRepository;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static uz.telsale.telsale.domain.dtos.response.Status.ERROR;
import static uz.telsale.telsale.domain.dtos.response.Status.SUCCESS;
import static uz.telsale.telsale.domain.user.State.BLOCKED;
import static uz.telsale.telsale.domain.user.State.UNVERIFIED;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthRepository authRepository;
    @Override
    public User create(UserDto userDto) {
        if (userRepository.existsUserByUsername(userDto.getUsername())) {
           throw new  DuplicateKeyException("User with the provided username already exists.");}
        if (userRepository.existsUserByPhoneNumber(userDto.getPhoneNumber())) {
            throw  new DuplicateKeyException("User with the provided phone number already exists.");
        }
        User user = modelMapper.map(userDto, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User getById(UUID id) {
        return userRepository.findById(id).orElseThrow(
                ()-> new DataNotFoundException("User not found by Id"));
    }

    @Override
    public User update(UserDto userDto, UUID id) {
        User user = getById(id);
        user.setUsername(userDto.getUsername());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPassword(userDto.getPassword());
        user.setRole(userDto.getRole());
        user.setState(userDto.getState());
        user.setUpdatedDate(LocalDateTime.now());
        return userRepository.save(user);
    }

    @Override
    public void deleteById(UUID id) {
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Response<?> register(UserDto user) {
        try{
        return Response.builder().status(SUCCESS).msg("Registered user: "+user.getPhoneNumber())
                .data(create(user)).build();
        } catch (Exception ex) {
            throw new UserRegistrationException("Something wrong with registration user: "+ex.getMessage());
        }
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findUserByUsername(username).orElseThrow(()-> new DataNotFoundException("User not found by Username"));
    }

    @Override
    public User getByPhoneNum(String phoneNumber) {
        return userRepository.findUserByPhoneNumber(phoneNumber).orElseThrow(()-> new DataNotFoundException("User not found by PhoneNumber"));
    }

    @Override
    public User getByPhoneNumberOrUsername(String numberOrUsername) {
        return userRepository.findUserByUsernameOrPhoneNumber(numberOrUsername).orElseThrow(()-> new DataNotFoundException("Username or PhoneNumber not found"));
    }

    @Override
    public Response<?> login(LoginDto loginDto){
        try {
            User user = getByPhoneNum(loginDto.getPhoneNumberOrUsername());
            if (passwordEncoder.matches(loginDto.getPassword(), user.getPassword())) {
                switch (user.getState()) {
                    case ACTIVE -> {
                        return Response.builder().status(SUCCESS)
                                .data(new JwtResponse<>(jwtService.generateAccessToken(user),
                                        jwtService.generateRefreshToken(user), null))
                                .msg("login").build();
                    }
                    case BLOCKED -> {
                        return Response.builder()
                                .status(ERROR).msg("User is blocked").data(BLOCKED).build();
                    }
                    case UNVERIFIED -> {
                        return Response.builder().status(ERROR).msg("User is UNVERIFIED").data(UNVERIFIED).build();
                    }
                    default -> throw new AuthenticationFailedException("Phone or password is wrong");
                }
            }
            throw new DataNotFoundException("Password is wrong");
        }catch (Exception e){
            throw new DataNotFoundException(e.getMessage());
        }
    }

    @Override
    public Response<?> getAllUser(PageList pageUserList) {
        try {
            Pageable pageable = pageableInShort(pageUserList.getPage(), pageUserList.getSize(), pageUserList.getSortBy(), Sort.Direction.valueOf(pageUserList.getSortDirection()));
            Page<User> userList = userRepository.findUserBy(pageable);
            return Response.builder()
                    .status(SUCCESS)
                    .msg("User list retrieved successfully")
                    .data(userList.getContent())
                    .build();
        } catch (Exception e) {
            throw new DataNotFoundException("Something wrong: "+e);
        }
    }

    @Override
    public Response<?> updateUser(UserDto userDto, UUID id, Principal principal) {
        try {
            User changer = getByUsername(principal.getName());
            User user = getById(id);
            if (user == null) {
                throw new DataNotFoundException("User not found with id: " + id);
            }
            if (!user.getId().equals(id) && !isAdmin(changer)) {
               throw new DataNotFoundException("Changer hasn't access to update user");
            }
            user.setUsername(userDto.getUsername());
            user.setPhoneNumber(userDto.getPhoneNumber());
            user.setRole(userDto.getRole());
            user.setState(userDto.getState());
            user.setUpdatedDate(LocalDateTime.now());
            User updatedUser = userRepository.save(user);
            return Response.builder()
                    .msg("User updated successfully")
                    .status(SUCCESS)
                    .data(updatedUser)
                    .build();
        } catch (DataNotFoundException ex) {
           throw new DataNotFoundException("Something is wrong");
        }
    }

    @Override
    public Response<?> getUserInfo(Principal principal) {
        try {
            User user = getByUsername(principal.getName());
            if (user == null) {
                throw new DataNotFoundException("User not found with username: " + principal.getName());
            }
            return Response.builder()
                    .status(SUCCESS)
                    .msg("Get user-info")
                    .data(user)
                    .build();
        } catch (Exception ex) {
           throw new DataNotFoundException("Something wrong: "+ex);
        }
    }

    @Override
    public Response<?> deleteUser(UUID userId, Principal principal) {
        try {
            User user = getByUsername(principal.getName());
            User userToDelete = getById(userId);
            if (userToDelete == null) {
                throw new DataNotFoundException("User not found with id: " + userId);
            }
            if (!user.getId().equals(userId) && !isAdmin(user)) {
                return Response.builder()
                        .msg("Unauthorized to delete user")
                        .status(Status.ERROR)
                        .data(null)
                        .build();
            }
            userRepository.deleteById(userId);
            return Response.builder()
                    .status(SUCCESS)
                    .msg("User deleted: " + userToDelete.getUsername())
                    .data(null)
                    .build();
        } catch (DataNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Response<?> newTokenByOld(String token) {
        try {
            User user = jwtService.getUserByToken(token);
            JwtResponse<Object> jwtResponse = JwtResponse.builder()
                    .accessToken(jwtService.generateAccessToken(user))
                    .refreshToken(jwtService.generateRefreshToken(user))
                    .data(user)
                    .build();

            return Response.builder()
                    .status(SUCCESS)
                    .msg("New token")
                    .data(jwtResponse)
                    .build();
        } catch (DataNotFoundException ex) {
            return Response.builder()
                    .status(Status.ERROR)
                    .msg("Token does not have data about the user or user not found")
                    .data(ex.getMessage())
                    .build();
        } catch (Exception e) {
            return Response.builder()
                    .status(Status.ERROR)
                    .msg("An error occurred while processing the token")
                    .data(e.getMessage())
                    .build();
        }
    }

    @Override
    public Response<?> setNewPassword(String phone, String newPassword) {
        try {
            User byUsernameOrPhoneNumber = getByPhoneNumberOrUsername(phone);
            byUsernameOrPhoneNumber.setPassword(passwordEncoder.encode(newPassword));
            byUsernameOrPhoneNumber.setUpdatedDate(LocalDateTime.now());
            userRepository.save(byUsernameOrPhoneNumber);
            return Response.builder()
                    .status(SUCCESS)
                    .msg("Password changed successfully")
                    .build();
        } catch (Exception e) {
            throw new DataNotFoundException(e.getMessage());}
    }

    @Override
    public Response<?> verifyUser(String phoneNumber, int code) {
        try {
            User user = getByPhoneNum(phoneNumber);
            if (isCode(user.getId(), code)) {
                user.setState(State.ACTIVE);
                userRepository.save(user);
                return Response.builder()
                        .status(SUCCESS)
                        .msg("User verified successfully")
                        .build();
            } else {
                return Response.builder()
                        .status(Status.ERROR)
                        .msg("Invalid verification code")
                        .build();
            }
        } catch (DataNotFoundException e) {
            return Response.builder()
                    .status(Status.ERROR)
                    .msg("User not found")
                    .data(e.getMessage())
                    .build();
        } catch (Exception e) {
            return Response.builder()
                    .status(Status.ERROR)
                    .msg("An error occurred while verifying the user")
                    .data(e.getMessage())
                    .build();
        }
    }

    @Override
    public Response<?> sendVerifyCode(String phoneNumber) {
        User user = getByPhoneNum(phoneNumber);
        AuthEntity authEntity = AuthEntity.builder().userId(user.getId()).verificationCode(codeGenerate()).build();
        authRepository.save(authEntity);
        return Response.builder().status(SUCCESS).msg("Message sent to "+phoneNumber).build();
    }

    @Override
    public Response<?> getUserInfoById(UUID userId) {
        User user = getById(userId);
        return Response.builder().status(SUCCESS).msg("Get userInfo by userId").data(user).build();
    }


    private Pageable pageableInShort(int page, int size, String sortField, Sort.Direction sortDirection){
        Sort sort = Sort.by(sortDirection, sortField);
        return PageRequest.of(page, size, sort);
    }
    private Boolean isAdmin(User user){
        return Role.ADMIN.equals(user.getRole());
    }



    private int codeGenerate(){
        Random random = new Random(100000);
        return random.nextInt(1000000);
    }

    private Boolean isCode(UUID userId, int code){
        AuthEntity authEntity = authRepository.findAuthEntitiesByUserIdAndVerificationCode(userId, code);
        authRepository.deleteById(authEntity.getId());
        return true;
    }

    private boolean isValidSortField(String sortField) {
        List<String> allowedFields = Arrays.asList("id", "username", "password", "created_date", "updated_date", "phone_number", "role", "state");
        return allowedFields.contains(sortField);
    }

    private boolean isValidSortDirection(String sortDirection) {
        return sortDirection.equalsIgnoreCase("ASC") || sortDirection.equalsIgnoreCase("DESC");
    }
}
