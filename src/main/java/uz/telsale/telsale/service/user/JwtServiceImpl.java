package uz.telsale.telsale.service.user;

import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import uz.telsale.telsale.domain.user.User;
import uz.telsale.telsale.exeptions.DataNotFoundException;
import uz.telsale.telsale.repository.TokenRepository;
import uz.telsale.telsale.repository.UserRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService{
    @Value("${jwt.secret.key}")
    private String secretKey;
    @Value("${jwt.access.expiry}")
    private long accessTokenExpiry;
    @Value("${jwt.refresh.expiry}")
    private long refreshToken;

    private final TokenRepository tokenRepository;
    private final UserRepository userRepository;

    @Override
    public String generateAccessToken(User userEntity) {
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS512,secretKey)
                .setSubject(userEntity.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime()+accessTokenExpiry))
                .addClaims(Map.of("role",getRoles(userEntity.getAuthorities())))
                .compact();
    }

    @Override
    public String generateRefreshToken(User userEntity) {
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setSubject(userEntity.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + refreshToken))
                .compact();
    }

    @Override
    public User getUserByToken(String token) {
        try{
            Jws<Claims> jws = extractToken(token);
            Claims claims = jws.getBody();
            String sub = claims.get("sub",String.class);
            return userRepository.findUserByUsername(sub).orElseThrow(()-> new DataNotFoundException("User not found"));
        }catch (ExpiredJwtException e){
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Jws<Claims> extractToken(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
    }
    private List<String> getRoles(Collection<?extends GrantedAuthority> roles){
        List<String> list = roles.stream()
                .map(GrantedAuthority::getAuthority)
                .toList();
        return list;
    }
}
