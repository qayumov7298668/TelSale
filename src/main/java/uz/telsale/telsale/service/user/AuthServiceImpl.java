package uz.telsale.telsale.service.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements  UserDetailsService {
    private final UserService userService;
    @Override
    public UserDetails loadUserByUsername(String phoneNumOrUsername) {
        return userService.getByPhoneNumberOrUsername(phoneNumOrUsername);
    }
}
