package uz.telsale.telsale.service.user;

import org.springframework.security.core.userdetails.UserDetails;

public interface AuthService {
    UserDetails loadUserByUsername(String phoneNumber);
}
