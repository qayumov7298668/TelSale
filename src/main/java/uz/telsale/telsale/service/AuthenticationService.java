package uz.telsale.telsale.service;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

public interface AuthenticationService {
    void Authenticate(Claims claims, HttpServletRequest request);
    List<SimpleGrantedAuthority> getRoles(List<String>roles);
}
