package uz.telsale.telsale.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import uz.telsale.telsale.filter.JwtFilterToken;
import uz.telsale.telsale.service.AuthenticationService;
import uz.telsale.telsale.service.user.AuthServiceImpl;
import uz.telsale.telsale.service.user.JwtService;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfig{

    private final AuthServiceImpl authService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationService authenticationService;
    private final JwtService jwtService;
    private static final String[] WISHLIST = {"/auth/**",
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/docs","/v2/api-docs",
            "/hello","/sendSMS", "/product/list","/"};
    private final String[] RESOURCES={"/static/upload/**"};

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        return http
                .csrf().disable()
                .authorizeHttpRequests((authorize) -> {
                    authorize
                            .requestMatchers(WISHLIST).permitAll()
                            .requestMatchers(RESOURCES).permitAll()
                            .requestMatchers("/user/**", "/user").hasAnyRole("USER", "ADMIN")
                            .requestMatchers("/admin/**", "/admin").hasAnyRole("ADMIN")
                            .anyRequest().authenticated();
                })
//                .oauth2ResourceServer(customizer-> customizer.opaqueToken(withDefaults()))
                .sessionManagement((session) -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilterBefore(new JwtFilterToken(authenticationService, jwtService), UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling(customizer -> customizer.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)))
                .build();
    }

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity httpSecurity) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder =
                httpSecurity.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder
                .userDetailsService(authService)
                .passwordEncoder(passwordEncoder);
        return authenticationManagerBuilder.build();
    }

//    @Bean
//    public OpaqueTokenIntrospector introspector(){
//        return new GoogleOpaqueTokenInspektor(userInfoClient);
//    }
}
