package uz.telsale.telsale.config;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.domain.dtos.response.Status;
import uz.telsale.telsale.exeptions.*;

import java.nio.file.AccessDeniedException;
import java.util.*;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {RequestValidationException.class})
    public ResponseEntity<Response<String>>
    requestValidationExceptionHandler(
            RequestValidationException e
    ){
        return ResponseEntity.status(400).body(Response.<String>builder().status(Status.ERROR).msg(e.getMessage()).build());
    }

    @ExceptionHandler(value = {AuthenticationFailedException.class})
    public ResponseEntity<Response<String>> authenticationFailedExceptionHandler(
            AuthenticationFailedException e
    ){
        return ResponseEntity.status(401).body(Response.<String>builder().status(Status.ERROR).msg(e.getMessage()).build());
    }
    @ExceptionHandler(value = {AccessDeniedException.class})
    public ResponseEntity<Response<String>> AccessDeniedExceptionHandler(
            AccessDeniedException e
    ){
        return ResponseEntity.status(403).body(Response.<String>builder().status(Status.ERROR).msg(e.getMessage()).build());
    }

    @ExceptionHandler(value = {DataNotFoundException.class})
    public ResponseEntity<Response<String>> dataNotFoundExceptionHandler(
            DataNotFoundException e){
        return ResponseEntity.status(404).body(Response.<String>builder().status(Status.ERROR).msg(e.getMessage()).build());

    }

    @ExceptionHandler(value = {UniqueObjectException.class})
    public ResponseEntity<Response<String>> uniqueObjectExceptionHandler(
            UniqueObjectException e){
        return ResponseEntity.status(401).body(Response.<String>builder().status(Status.ERROR).msg(e.getMessage()).build());
    }
    @ExceptionHandler(value = {UserBadRequestException.class})
    public ResponseEntity<Response<String>> userBadRequestExceptionHandler(
            UserBadRequestException e){
        return ResponseEntity.status(400).body(Response.<String>builder().status(Status.ERROR).msg(e.getMessage()).build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Response<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request) {
        String errorMessage = "Input is not valid";
        Map<String, List<String>> errorBody = new HashMap<>();
        for (FieldError fieldError : e.getFieldErrors()) {
            String field = fieldError.getField();
            String msg = fieldError.getDefaultMessage();
            errorBody.compute(field, (s, values) -> {
                if (!Objects.isNull(values))
                    values.add(msg);
                else
                    values = new ArrayList<>(Collections.singleton(msg));
                return values;
            });
        }
        String errorPath = request.getRequestURI();
        return Response.<String>builder()
                .status(Status.valueOf(errorPath))
                .msg(errorMessage)
                .data(String.valueOf(errorBody))
                .build();
    }
}
