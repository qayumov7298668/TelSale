package uz.telsale.telsale.config;


import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.web.reactive.function.client.WebClient;
import uz.telsale.telsale.domain.dtos.UserDto.UserInfo;

import java.util.HashMap;
import java.util.Map;

public class GoogleOpaqueTokenInspektor implements OpaqueTokenIntrospector {

    private final WebClient userInfoCleint;

    public GoogleOpaqueTokenInspektor(WebClient userInfoCleint) {
        this.userInfoCleint = userInfoCleint;
    }

    @Override
    public OAuth2AuthenticatedPrincipal introspect(String token) {
        UserInfo userInfo = userInfoCleint.get().uri(uriBuilder -> uriBuilder.path("/oauth2/v3/userInfo").queryParam("/access_token", token).build())
                .retrieve()
                .bodyToMono(UserInfo.class)
                .block();

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", userInfo.sub());
        attributes.put("name", userInfo.name());
        return new OAuth2IntrospectionAuthenticatedPrincipal(userInfo.name(), attributes, null);
    }
}
