package uz.telsale.telsale;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "TelSale APIS", version = "1.0", description = "TelSale Management Apis"))
public class TelSaleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelSaleApplication.class, args);
    }

}
