package uz.telsale.telsale.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.telsale.telsale.domain.dtos.ProductDto;
import uz.telsale.telsale.domain.dtos.UserDto.PageList;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.service.product.FileService;
import uz.telsale.telsale.service.product.ProductService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
@Tag(name = "Products", description = "Changes product(ProductController)")
public class ProductController {
    private final ProductService productService;
    private final FileService fileUpload;


    @GetMapping("/{productId}")
    @Operation(summary = "Product information")
    public Response<?> getProduct(@PathVariable UUID productId){
       return productService.getProductInfo(productId);
    }

    @GetMapping("/list")
    @Operation(summary = "Pageable products list")
    public Response<?> productList(@RequestParam(defaultValue = "0", name = "page") int page,
                                   @RequestParam(defaultValue = "10", name = "size") int size,
                                   @RequestParam(defaultValue = "createdDate", name = "sortBy") String sortBy,
                                   @RequestParam(defaultValue = "DESC", name = "sortDirection") String sortDirection){
        PageList list = new PageList(page, size, sortBy, sortDirection);
        return productService.getPageProductList(list);
    }

    @PostMapping(value = "/add")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @Operation(summary = "Add product")
    public Response<?> addProduct(
            @RequestBody ProductDto productDto,
            Principal principal
    ) {
        return productService.addProduct(productDto, principal);
    }

    @PostMapping(value = "/add/images", consumes = "multipart/form-data")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @Operation(summary = "Add product images by productId")
    public Response<?> addProductPhoto(
            @RequestBody List<MultipartFile> productImage,
            @RequestParam UUID productId,
            Principal principal
    ){
        return productService.saveProductImages(productImage, productId,principal);
    }



    @GetMapping("/img/{imageName}")
    @Operation(summary = "Get image by unique name")
    public ResponseEntity<?> productImage(
            @PathVariable String imageName
            ){
        Path file = fileUpload.getFile(imageName);
        try {
            String mediaType = Files.probeContentType(file);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(mediaType))
                    .body(new UrlResource(file.toUri()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @GetMapping("/my-products")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @Operation(summary = "Owner products")
    public Response<?> getOwnerPage(@RequestParam PageList pageList,
                                    Principal principal){
        return productService.getPageOwnerProduct(pageList, principal);
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @Operation(summary = "Delete product with productId")
    public Response<?> deleteProduct(@RequestParam UUID productId,Principal principal){
        return productService.deleteProduct(productId, principal);
    }


}
