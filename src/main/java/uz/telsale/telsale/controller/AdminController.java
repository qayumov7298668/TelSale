package uz.telsale.telsale.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import uz.telsale.telsale.domain.dtos.UserDto.PageList;
import uz.telsale.telsale.domain.dtos.UserDto.UserDto;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.service.user.UserService;

import java.security.Principal;
import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
@Tag(name = "Admin ", description = "Dashboard(AdminController)")
public class AdminController {
    private final UserService userService;

    @PostMapping("/users")
    @Operation(summary = "Pageable users list")
    public Response<?> getAllUserPage(
            @RequestBody PageList page
            ){
        return userService.getAllUser(page);
    }

    @PatchMapping("/user/update")
    @Operation(summary = "Update user by userID")
    public Response<?> updateUser(@RequestBody UserDto userDto,@RequestParam UUID id,
                                  Principal principal){
       return userService.updateUser(userDto,id,principal);
    }

    @GetMapping("/user/info")
    @Operation(summary = "Show user information by userID")
    public Response<?> getUserInfo(
            @RequestParam UUID userId
    ){
        return userService.getUserInfoById(userId);
    }

    @GetMapping()
    @Operation(summary = "Show admin information")
    public Response<?> getUserInfo(
            Principal principal
    ){
        if (principal instanceof Authentication) {
            Collection<? extends GrantedAuthority> authorities = ((Authentication) principal).getAuthorities();
            System.out.println("Authorities: " + authorities);
        }
        return userService.getUserInfo(principal);
    }
}
