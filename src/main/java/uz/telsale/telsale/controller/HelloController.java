package uz.telsale.telsale.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HelloController {

    @GetMapping("/hello")
    @Operation(summary = "Test for working server")
    public String hello(){
        return "hello world";
    }

}
