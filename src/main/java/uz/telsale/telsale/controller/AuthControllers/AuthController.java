package uz.telsale.telsale.controller.AuthControllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import uz.telsale.telsale.domain.dtos.UserDto.LoginDto;
import uz.telsale.telsale.domain.dtos.UserDto.UserDto;
import uz.telsale.telsale.domain.dtos.UserDto.VerificateCodeDto;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.service.user.UserService;

import javax.naming.AuthenticationException;

@RestController
@RequestMapping("/auth")
@Tag(name = "Authorization", description = "Login and Register(AuthController)")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;

    @PostMapping("/sign-up")
    @Operation(summary = "Registration user")
    public Response<?> signUp(@Valid @RequestBody UserDto userDto) {
        return userService.register(userDto);
    }

    @PostMapping("/login")
    @Operation(summary = "Login with Login and Password")
    public Response<?> login(@RequestBody LoginDto loginDto) throws AuthenticationException {
        return userService.login(loginDto);
    }

    @PostMapping("/send-verify-code")
    @Operation(summary = "Send verification code to phone")
    public Response<?> sendVerifyCode(
            @RequestBody String phoneNumber
    ){
        return userService.sendVerifyCode(phoneNumber);
    }

    @PostMapping("/verify")
    @Operation(summary = "Verify code")
    public Response<?> verifyCode(
            @RequestBody VerificateCodeDto verificateCodeDto
            ){
        return userService.verifyUser(verificateCodeDto.getPhoneNumber(), verificateCodeDto.getVerifyCode());
    }

    @PutMapping("/change-password")
    @Operation(summary = "Change password by login")
    public Response<?> saveNewPassword(
            @RequestBody LoginDto loginDto
    ){
        return userService.setNewPassword(loginDto.getPhoneNumberOrUsername(), loginDto.getPassword());
    }

    @GetMapping("/new-token")
    @Operation(summary = "Get new token from old token")
    public Response<?> newTokenByOld(
            @RequestBody String oldToken
            ){
        return userService.newTokenByOld(oldToken);}


}
