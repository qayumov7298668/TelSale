package uz.telsale.telsale.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.telsale.telsale.domain.dtos.UserDto.UserDto;
import uz.telsale.telsale.domain.dtos.response.Response;
import uz.telsale.telsale.service.user.UserService;

import java.security.Principal;
import java.util.UUID;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Tag(name = "User info", description = "User changes(UserController)")
public class UserController {
    private final UserService userService;

    @GetMapping()
    @Operation(summary = "Get user info")
    public Response<?> getUser(Principal principal){
        return userService.getUserInfo(principal);
    }

    @PatchMapping("/update")
    @Operation(summary = "Update user")
    public Response<?> updateUser(@Valid @RequestBody UserDto userDto,
                                  @RequestParam UUID id, Principal principal){
        return userService.updateUser(userDto, id, principal);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "Delete user with userID")
    public Response<?> delete(@RequestParam UUID id, Principal principal){
        return userService.deleteUser(id, principal);
    }
}
