package uz.telsale.telsale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.telsale.telsale.domain.FileEntity;

import java.util.UUID;

@Repository
public interface FileUploadRepository extends JpaRepository<FileEntity, UUID> {

}
