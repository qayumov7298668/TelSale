package uz.telsale.telsale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.telsale.telsale.domain.AuthEntity;

import java.util.UUID;
@Repository
public interface AuthRepository extends JpaRepository<AuthEntity, UUID> {
    AuthEntity findAuthEntitiesByUserIdAndVerificationCode(UUID userId, int code);
}
