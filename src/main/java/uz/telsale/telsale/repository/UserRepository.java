package uz.telsale.telsale.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.telsale.telsale.domain.user.User;

import java.util.Optional;
import java.util.UUID;
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findUserByUsername(String username);
    Optional<User> findUserByPhoneNumber(String number);
    Page<User> findUserBy(Pageable pageable);
    @Query("SELECT u FROM telsale_users u WHERE u.username = :value OR u.phoneNumber = :value")
    Optional<User> findUserByUsernameOrPhoneNumber(String value);

    Boolean existsUserByPhoneNumber(String phoneNumber);
    Boolean existsUserByUsername(String username);

}
