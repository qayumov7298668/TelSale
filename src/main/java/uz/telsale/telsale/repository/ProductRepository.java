package uz.telsale.telsale.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.telsale.telsale.domain.Product;

import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {
    Page<Product> findAllBy(Pageable pageable);
    Page<Product> findProductByOwnerId(UUID ownerId, Pageable pageable);
}
