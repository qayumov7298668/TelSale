package uz.telsale.telsale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.telsale.telsale.domain.JwtTokenEntity;

import java.util.UUID;
@Repository
public interface TokenRepository extends JpaRepository<JwtTokenEntity, UUID> {

}
