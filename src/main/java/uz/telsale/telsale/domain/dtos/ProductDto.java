package uz.telsale.telsale.domain.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private String name;
    private String description;
    private int storage;
    private String condition;
    private int batteryHealth;
    private String color;
    private Boolean box;
    private int price;
    private Boolean exchange;
    private String customerPhone;
    private String customerTg;
    private String address;
}
