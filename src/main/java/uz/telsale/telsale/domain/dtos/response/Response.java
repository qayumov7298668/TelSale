package uz.telsale.telsale.domain.dtos.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Response <T> {
    private Status status;
    private String msg;
    private T data;
}
