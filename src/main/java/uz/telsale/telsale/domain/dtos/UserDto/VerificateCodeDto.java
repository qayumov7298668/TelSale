package uz.telsale.telsale.domain.dtos.UserDto;

import lombok.Data;

@Data
public class VerificateCodeDto {
    private String phoneNumber;
    private int verifyCode;
}
