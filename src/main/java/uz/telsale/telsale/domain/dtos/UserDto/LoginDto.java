package uz.telsale.telsale.domain.dtos.UserDto;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDto {
    @NotBlank(message = "login is null")
    private String PhoneNumberOrUsername;
    @NotBlank(message = "password is null")
    private String password;
}
