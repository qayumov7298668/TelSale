package uz.telsale.telsale.domain.dtos.UserDto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import uz.telsale.telsale.domain.user.Role;
import uz.telsale.telsale.domain.user.State;

@Data
public class UserDto {
    @NotBlank(message = "username can not be null")
    @NotNull
    private String username;
    @NotBlank(message = "phoneNumber can not be null")
    @NotNull
    private String phoneNumber;
    @NotBlank(message = "password can not be null")
    @NotNull
    private String password;
    private Role role=Role.USER;
    private State state=State.UNVERIFIED;
}
