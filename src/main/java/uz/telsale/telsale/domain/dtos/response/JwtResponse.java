package uz.telsale.telsale.domain.dtos.response;

import lombok.*;
import uz.telsale.telsale.domain.user.User;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class JwtResponse <T>{
    private String accessToken;
    private String refreshToken;
    private T data;
}