package uz.telsale.telsale.domain.dtos.response;

public enum Status {
    SUCCESS,
    ERROR
}
