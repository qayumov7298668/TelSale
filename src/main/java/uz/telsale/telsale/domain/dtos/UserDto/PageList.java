package uz.telsale.telsale.domain.dtos.UserDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PageList {
    private int page=0;
    private int size=10;
    private String sortBy="createdDate";
    private String sortDirection="DESC";
}
