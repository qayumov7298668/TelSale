package uz.telsale.telsale.domain;

import jakarta.persistence.Entity;
import lombok.*;

import java.util.UUID;

@Entity(name = "auth_codes")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthEntity extends BaseEntity{
    private UUID userId;
    private int verificationCode;
}
