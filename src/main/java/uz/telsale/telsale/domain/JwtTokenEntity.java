package uz.telsale.telsale.domain;

import jakarta.persistence.Entity;
import lombok.*;

import java.time.LocalDateTime;

@Entity(name = "jwt-token")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JwtTokenEntity extends BaseEntity{
    private String token;
    private LocalDateTime expireDate;
}
