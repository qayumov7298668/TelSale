package uz.telsale.telsale.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.Positive;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Entity(name = "product")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product extends BaseEntity {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<FileEntity> images;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "storage")
    @Positive(message = "Storage must be a positive value")
    private int storage;

    @Column(name = "condition")
    private String condition;

    @Column(name = "battery_health")
    @Positive(message = "Battery must be a positive value")
    private int batteryHealth;

    @Column(name = "color")
    private String color;

    @Column(name = "box")
    private boolean box;

    @Column(name = "price")
    private int price;

    @Column(name = "exchange")
    private boolean exchange;

    @Column(name = "customer_phone")
    private String customerPhone;

    @Column(name = "customer_tg")
    private String customerTg;

    @Column(name = "address")
    private String address;

    @Column(name = "owner_id")
    private UUID ownerId;
}
