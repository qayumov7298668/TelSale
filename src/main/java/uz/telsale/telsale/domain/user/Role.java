package uz.telsale.telsale.domain.user;

public enum Role {
    USER,
    ADMIN,
    MANAGER
}
