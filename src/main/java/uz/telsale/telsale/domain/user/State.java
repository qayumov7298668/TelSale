package uz.telsale.telsale.domain.user;

public enum State {
    BLOCKED,
    ACTIVE,
    UNVERIFIED
}
