package uz.telsale.telsale.exeptions;

public class DuplicateKeyException extends RuntimeException {
    public DuplicateKeyException(String msg) {
        super(msg);
    }
}
