package uz.telsale.telsale.exeptions;

public class UniqueObjectException extends RuntimeException{

    public UniqueObjectException(String message) {
        super(message);
    }
}
