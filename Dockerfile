FROM openjdk:17

EXPOSE 8083
ARG JAR_FILE=build/libs/TelSale-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} telsale-docker

ENTRYPOINT ["java", "-jar", "telsale-docker"]